using System;
using UnityEngine;

public class ARPoint
{
    public ulong ID { get; set; } 
    public Vector3 POS { get; set; }
    public float CONF { get; set; }
    // public ARPoint(Vector3 pos)
    // {
    //     this.id = id;
    //     this.pos = pos;
    //     this.conf = conf;
    // }
}
