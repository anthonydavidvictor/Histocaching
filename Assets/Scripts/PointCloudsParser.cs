using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine.Networking;
using UnityEngine.XR.ARFoundation;

public class PointCloudsParser : MonoBehaviour
{
    public ARPointCloudManager pointCloudManager;
    private List<ARPoint> arPoints = new List<ARPoint>();
    private List<ARPoint> updPoints = new List<ARPoint>();
    private const string URL = "http://192.168.1.193:9000/Server.js";


    private void OnEnable()
    {
        pointCloudManager.pointCloudsChanged += PointCloudManager_pointCloudsChanged;
        
    }

    private void PointCloudManager_pointCloudsChanged(ARPointCloudChangedEventArgs obj)
    {


        foreach (var pointCloud in obj.added)
        {
            ARPoint newPoint = new ARPoint();
            foreach (var pos in pointCloud.positions)
            {
                newPoint.POS = pos;
            }
            // foreach (var conf in pointCloud.confidenceValues)
            // {
            //     newPoint.CONF = conf;
            // }
            foreach (var id in pointCloud.identifiers)
            {
                newPoint.ID = id;
            }
            arPoints.Add(newPoint);
        }
        foreach (var pointCloud in obj.updated)
        {
            ARPoint newPoint = new ARPoint();
            foreach (var pos in pointCloud.positions)
            {
                newPoint.POS = pos;
            }
            // foreach (var conf in pointCloud.confidenceValues)
            // {
            //     newPoint.CONF = conf;
            // }
            foreach (var id in pointCloud.identifiers)
            {
                newPoint.ID = id;
            }
            updPoints.Add(newPoint);
        }

        
    }
    public void InvokeUploadPNG()
    {
        StartCoroutine(UploadPNG());
    } private IEnumerator UploadPNG()
    {
        string json = "";
        Dictionary<String, String> points =
            new Dictionary<String, String>();
        points.Add("Positions:","");
        points.Add("Id:", "");
        
        if (updPoints != null)
        {
            for (int i = 0; i <updPoints.Count; i++)
            {
                points["Positions:"] += updPoints[i].POS.ToString();
                points["Id:"] += updPoints[i].CONF.ToString();
            }
            json = JsonConvert.SerializeObject(points, Formatting.Indented);
        }
        using (var request = UnityWebRequest.Put(URL, json))
        {
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success) {
                Debug.Log(request.error);
            }
            else {
                Debug.Log("Finished Uploading Screenshot");
            }
        }
    }
}
        
        
    



    