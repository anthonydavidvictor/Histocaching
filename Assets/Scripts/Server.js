﻿"use strict";

var http = require("http");
var qs = require('querystring');

class Server
{
	constructor()
	{
		this.port = 9000;
		this.ip = "192.168.1.193";

		this.start();
	}

	start()
	{
		this.server = http.createServer((req, res) =>
		{
			this.processRequest(req, res);
		});

		this.server.on("clientError", (err, socket) =>
		{
			socket.end("HTTP/1.1 400 Bad Request\r\n\r\n");
		});
		console.log("Server created");
	}

	listen()
	{
		this.server.listen(this.port, this.ip);
		console.log("Server listening for connections");
	}

	processRequest(req, res)
	{
		if (req.method === "PUT")
		{
			var body = "";	
			req.on("data", (data) =>
			{
				body += data;
			});
			req.on("end", () =>
			{
				// Now that we have all data from the client, we process it
				// console.log("Received data: " + body);
				// Split the key / pair values and print them out
				const data = JSON.parse(body.toString());
				console.log(data);
				// Tell Unity that we received the data OK
				res.writeHead(200, {"Content-Type": "text/plain"});
				res.end("OK");
			});
		}
		else
		{
			// Tell Unity that the HTTP method was not allowed
			res.writeHead(405, "Method Not Allowed", {"Content-Type": "text/html"});
			res.end("Error 405");
		}
	}

}

var httpServer = new Server();
httpServer.listen();